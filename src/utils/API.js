import Axios from "axios"

//const DEV_API_URL = 'http://localhost:3001/api/v1';
const PROD_API_URL = 'https://ping-pong-sesh-api.herokuapp.com/api/v1';

const API = Axios.create({
  baseURL: PROD_API_URL,
  responseType: 'json'
});

export default API;