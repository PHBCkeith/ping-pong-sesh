import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Sesh from '../components/Sesh';
import SeshIndex from '../components/SeshIndex';
import PlayerIndex from '../components/PlayerIndex';
import Player from '../components/Player';
import AppHeader from '../components/AppHeader';
import NotFoundPage from '../components/NotFoundPage';

const AppRouter = () => (
  <BrowserRouter>
      <AppHeader />
      <div id="AppContainer">
        <Switch>
          <Route exact path="/" component={SeshIndex} />
          <Route path="/sesh/:id" component={Sesh} />
          <Route exact path="/players" component={PlayerIndex} />
          <Route path="/players/:id" component={Player} />
          <Route component={NotFoundPage} />
        </Switch>
      </div>
  </BrowserRouter>
)

export default AppRouter;