import { createStore, combineReducers, applyMiddleware } from "redux";
import seshReducer from "../reducers/sesh";
import seshesReducer from "../reducers/seshes";
import playersReducer from "../reducers/players";
import playerReducer from "../reducers/player";
import createSagaMiddleware from "redux-saga";
import IndexSagas from '../sagas/IndexSaga';
import { composeWithDevTools } from "redux-devtools-extension";

const sagaMiddleWare = createSagaMiddleware();

export default () => {
    const store = createStore(
      combineReducers({
          sesh: seshReducer,
          seshes: seshesReducer,
          players: playersReducer,
          player: playerReducer,
      }),
      composeWithDevTools(applyMiddleware(sagaMiddleWare))
    );
    sagaMiddleWare.run(IndexSagas);
    return store;
};