import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import FormStyles from '../styles/AddPlayerForm.module.css';
import SelectStyles from '../styles/ReactSelect.module.css';
import { ComboBox, Button } from 'carbon-components-react';
import downshiftStateReducer from '../reducers/downshiftStateReducer';
import { postMatch } from '../actions/sesh_actions';

const comboBoxProps = {
  className: SelectStyles.select,
  placeholder: "Filter...",
  itemToString: item => (item && item.text) || '',
  downshiftProps: {
    stateReducer: downshiftStateReducer
  },
};

const selectRefDefaultState = {
  state: {}
};

const SeshMatch = ({ sesh, dispatch }) => {
  const [ winnerSelectRef, setWinnerSelectRef ] = useState(selectRefDefaultState);
  const [ loserSelectRef, setLoserSelectRef] = useState(selectRefDefaultState);
  const [ winner, setWinner ] = useState({});
  const [ loser, setLoser ] = useState({});
  const [ winnerItems, setWinnerItems ] = useState([]);
  const [ loserItems, setLoserItems ] = useState([]);

  useEffect(() => {
    clearPlayers();
  }, [sesh.players]);

  const clearPlayers = () => {
    setWinner({});
    setLoser({});
  }

  useEffect(() => {
    setWinnerItems(
      sesh.players.filter((player) => player.name !== loserSelectRef.state.inputValue)
      .map((player) => ({ 
        id: player.id, 
        text: player.name,
      }))   
    );
    setLoserItems(
      sesh.players.filter((player) => player.name !== winnerSelectRef.state.inputValue)
      .map((player) => ({ 
        id: player.id, 
        text: player.name,
      }))   
    );
  }, [winner, loser, loserSelectRef.state.inputValue, winnerSelectRef.state.inputValue, sesh.players]);

  const onWinnerSelectChange = ({ selectedItem }) => {
    selectedItem ? 
      setWinner({ id: selectedItem.id, text: selectedItem.text }) :
      setWinner({ id: undefined, text: undefined });
  };

  const onLoserSelectChange = ({ selectedItem }) => {
    selectedItem ?
      setLoser({ id: selectedItem.id, text: selectedItem.text }) :
      setLoser({ id: undefined, text: undefined });
  };

  const onSubmitMatch = async (e) => {
    e.preventDefault();
    let submit_winner = sesh.players.find((player) => player.id === winner.id);
    let submit_loser = sesh.players.find((player) => player.id === loser.id);
    if (submit_winner && submit_loser && submit_winner.id !== submit_loser.id) {
      dispatch(postMatch(sesh.id, submit_winner, submit_loser));
      clearPlayers();
    }
  };

  const winnerSelectProps = {
    ...comboBoxProps,
    id: "winnerSelect",
    initialSelectedItem: winner,
    ref: select => setWinnerSelectRef(select),
    items: winnerItems,
    onChange: onWinnerSelectChange
  }

  const loserSelectProps = {
    ...comboBoxProps,
    id: "loserSelect",
    initialSelectedItem: loser,
    ref: select => setLoserSelectRef(select),
    items: loserItems,
    onChange: onLoserSelectChange
  }

  return (
    <form onSubmit={onSubmitMatch} className={FormStyles.form}>
      <ComboBox
        {...winnerSelectProps}
      />
      <strong>defeated</strong>
      <ComboBox
        {...loserSelectProps}
      />
      <Button size="field" type="submit" id="submitMatchButton" >submit match</Button>
    </form>
  );
};

const mapStateToProps = (state) => {
  return { 
    sesh: state.sesh,
    ...state.match
  };
};

export default connect(mapStateToProps)(SeshMatch);