import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { deleteSesh } from '../actions/seshes_actions';
import Sesh from './Sesh';
import { TableExpandRow, TableExpandedRow } from 'carbon-components-react';
import { TableCell } from 'carbon-components-react/lib/components/DataTable';

const SeshListRow = ({ row, dispatch, getRowProps }) => {

  const onDeleteSesh = () => {
    dispatch(deleteSesh(sesh_id()));
  }

  const sesh_id = () => parseInt(row.id);

  return (
    <React.Fragment key={sesh_id()}>
      <TableExpandRow {...getRowProps({ row })}>
        <TableCell key={'0'}>
          <Link to={`/sesh/${sesh_id()}`}>Sesh #{sesh_id()}</Link>
        </TableCell>
        <TableCell key={'1'}>
          <button onClick={onDeleteSesh}>Delete</button>
        </TableCell>
      </TableExpandRow>
      {row.isExpanded && (
        <TableExpandedRow colSpan={row.cells.length}>
          <Sesh sesh_id={sesh_id()} isPreview={true} />
        </TableExpandedRow>
      )}
    </React.Fragment>
  );
};

export default connect()(SeshListRow);