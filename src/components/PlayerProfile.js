import React from 'react';

const PlayerProfile = ({ player }) => {

  return (
    <>
      <h3>Player: {player.name}</h3>
      <p>Wins: {player.wins}</p>
      <p>Losses: {player.losses}</p>
    </>
  );
};

export default PlayerProfile;