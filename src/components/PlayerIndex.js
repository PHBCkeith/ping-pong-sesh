import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { loadPlayers } from '../actions/players_actions'
import { Link } from 'react-router-dom';
import { Button } from 'carbon-components-react';
import { deletePlayer } from '../actions/players_actions';
import AddPlayerForm from './AddPlayerForm';

const PlayerIndex = ({ players, dispatch }) => {
  
  useEffect(() => {
    dispatch(loadPlayers());
  }, [dispatch])

  const handleOnClickDelete = (player_id) => {
    if (window.confirm("Are you sure you wish to delete this player?")) {
      dispatch(deletePlayer(player_id));
    }
  }

  return (
    <>
      <AddPlayerForm />
      <table>
        <tbody> 
            {players.map((player, index) => 
              <tr key={index} style={{
                disiplay: 'flex',
                alignItems: 'center'
              }}>
                <td>
                  <Link 
                    to={`/players/${player.id}`} 
                    style={{ margin: '5px' }}
                  >{player.name}</Link>
                </td>
                <td>
                  <Button 
                    onClick={() => handleOnClickDelete(player.id)}
                    size="small"
                    style={{ margin: '5px' }}
                  >Delete Player</Button>
                </td>
              </tr>
            )}
        </tbody>
      </table>
    </>
  )
};

const mapStateToProps = (state) => ({
  players: state.players.players,
});

export default connect(mapStateToProps)(PlayerIndex);