import React from 'react';
import SeshListRow from './SeshListRow';
import { DataTable } from 'carbon-components-react';
import Styles from '../styles/Table.module.css';

const {
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableHeader,
  TableHead,
} = DataTable;
const headers = [
  {
    key: 'empty',
    header: ''
  },
  {
    key: 'name',
    header: 'Name'
  },
  {
    key: 'action',
    header: 'Action'
  }
];

const SeshList = ({ seshes }) => {

  const loadRows = () => {
    return seshes.seshes.map((sesh) => ({
      id: `${sesh.id}`
    })) || [];
  }

  return (
    <DataTable
      rows={loadRows()}
      headers={headers}
      render={({ rows, getTableProps, getHeaderProps, getRowProps }) => (
        <TableContainer title="Seshes Datatable" className={Styles.table}>
          <Table {...getTableProps()}>
            <TableHead>
              <TableRow>
                {headers.map((header) => 
                  <TableHeader {...getHeaderProps({ header })}>
                    {header.header}
                  </TableHeader>
                )}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row, index) =>
                <SeshListRow key={index} row={row} getRowProps={getRowProps} />
              )}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    />
  );
};

export default SeshList;