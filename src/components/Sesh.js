import React, { useEffect } from 'react';
import AddPlayerToSeshForm from './AddPlayerToSeshForm';
import SeshLeaderboard from './SeshLeaderboard';
import SeshMatch from './SeshMatch';
import { connect } from 'react-redux';
import { loadSesh, clearSeshState } from '../actions/sesh_actions';


const Sesh = ({ dispatch, ...props }) => {
  const sesh_id = props.sesh_id || props.match.params.id;
  // if the sesh_id was passed in via props, then this is a preview and not the full page
  const isPreview = !!props.sesh_id
  useEffect (() => {
    if (!isPreview) {
      dispatch(loadSesh(sesh_id));
      return () => dispatch(clearSeshState());
    }
  });

  return (
    <>
      {!isPreview && <h2>Sesh # {sesh_id}</h2>}
      {!isPreview && <AddPlayerToSeshForm />}
      <SeshLeaderboard sesh_id={props.sesh_id}/>
      {!isPreview && <SeshMatch />}
    </>
  );
};

export default connect()(Sesh);