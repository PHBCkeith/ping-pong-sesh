import React, { useState } from 'react'
import { connect } from 'react-redux';
import FormStyles from '../styles/AddPlayerForm.module.css';
import { Button } from 'carbon-components-react';
import { postPlayer } from '../actions/players_actions';

const defaultNewPlayerName = '';

const AddPlayerForm = ({ dispatch }) => {
  const [newPlayerName, setNewPlayerName] = useState(defaultNewPlayerName);

  const handleOnSubmit = (e) => {
    e.preventDefault();
    if (newPlayerName) {
      dispatch(postPlayer(newPlayerName));
    }
    setNewPlayerName(defaultNewPlayerName);
  }

  return (
    <>
    <form className={FormStyles.form} onSubmit={handleOnSubmit}>
      <label>New Player Name:</label>
      <input 
        type="text" 
        name="name" 
        value={newPlayerName}
        onChange={e => setNewPlayerName(e.target.value)}
      />
      <Button size="field" type="submit">Create Player</Button>
    </form>
    </>
  );
}

export default connect()(AddPlayerForm);