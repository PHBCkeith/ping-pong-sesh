import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { deleteSeshPlayer } from '../actions/sesh_actions';
import { TableRow, TableCell, Button } from 'carbon-components-react';

const SeshLeaderboardRow = ({ row, player,
                            sesh, dispatch, getRowProps, ...props }) => {
  const sesh_id = props.sesh_id || sesh.id;
  // if the sesh_id was passed in via props, this is a preview
  const isPreview = !!props.sesh_id;

  const onRemovePlayerClick = () => {
    if (window.confirm("Are you sure you wish to remove this player from the sesh?")) {
      dispatch(deleteSeshPlayer(sesh_id, player.id));
    }
  }

  const player_id = () => parseInt(row.id);

  return (
    <TableRow {...getRowProps({ row })}>
      <TableCell>
        <Link to={`/players/${player_id()}`}>{player.name}</Link>
      </TableCell>
      <TableCell>{player.wins}</TableCell>
      <TableCell>{player.losses}</TableCell>
      {!isPreview && 
        <TableCell>
          <Button 
            size="small"
            style={{padding: 0}}
            onClick={onRemovePlayerClick}>
            <strong style={{ padding: '0 10px 0 10px'}}>-</strong>
          </Button>
        </TableCell>}
    </TableRow>
  );
};

const mapStateToProps = (state, props) => {
  return {
    sesh: state.seshes.seshes.find((sesh) => sesh.id === props.sesh_id) || state.sesh
  };
};

export default connect(mapStateToProps)(SeshLeaderboardRow);