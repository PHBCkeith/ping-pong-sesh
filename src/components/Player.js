import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { loadPlayer, clearPlayerState } from '../actions/player_actions';
import PlayerProfile from './PlayerProfile';
import loadingGif from '../images/ring-loader.gif';

const Player = ({ dispatch, player, ...props }) => {
  const player_id = props.match.params.id;

  useEffect(() => {
    dispatch(loadPlayer(player_id));
    return () => dispatch(clearPlayerState());
  }, [ dispatch, player_id ]);

  return (
    <div>
      {player.status === 'completed' ?
        <PlayerProfile player={player} /> :
        <img src={loadingGif} alt="loading" height="100" width="100" />}
    </div>
  );
};

const mapStateToProps = (state) => ({
  player: state.player
});

export default connect(mapStateToProps)(Player);