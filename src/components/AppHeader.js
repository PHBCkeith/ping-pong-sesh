import React from 'react'
import { Link } from 'react-router-dom'
import { 
  Header, 
  HeaderName, 
  SkipToContent, 
  HeaderNavigation, 
  HeaderMenuItem, 
} from 'carbon-components-react/lib/components/UIShell';

const AppHeader = () => {

  const headerStyle = {
    position: 'relative'
  };

  return (
    <>
      <Header aria-label="Ping Pong Sesh App" style={headerStyle}>
        <SkipToContent />
        <HeaderName to="/" element={Link} prefix=''>
          Ping Pong Sesh
        </HeaderName>
        <HeaderNavigation aria-label="Ping Pong Sesh Navigation">
          <HeaderMenuItem to="/" element={Link}>Seshes</HeaderMenuItem>
          <HeaderMenuItem to="/players" element={Link}>Players</HeaderMenuItem>
        </HeaderNavigation>
      </Header>
    </>
  )
};

export default AppHeader;