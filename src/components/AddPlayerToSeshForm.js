import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { postSeshPlayer } from '../actions/sesh_actions';
import { loadPlayers } from '../actions/players_actions';
import FormStyles from '../styles/AddPlayerForm.module.css';
import SelectStyles from '../styles/ReactSelect.module.css';
import { ComboBox, Button, Form } from 'carbon-components-react';
import downshiftStateReducer from '../reducers/downshiftStateReducer';

const defaultNewPlayer = {};

const AddPlayerToSeshForm = ({ sesh, dispatch, players }) => {
  const [newPlayer, setNewPlayer] = useState(defaultNewPlayer);

  useEffect(() => {
    dispatch(loadPlayers());
  }, [dispatch]);

  const onSubmit = (e) => {
    e.preventDefault();
    dispatch(postSeshPlayer(newPlayer.text, sesh.id));
    setNewPlayer(defaultNewPlayer);
  }

  const getPlayerOptions = () => {
    return players.map((player) => ({ id: player.id, text: player.name}));
  }

  const handleOnSelectChange = ({ selectedItem }) => {
    setNewPlayer(selectedItem || defaultNewPlayer);
  }

  return (
    <Form onSubmit={onSubmit} className={FormStyles.form}>
      <ComboBox 
        id="addPlayerSelect"
        initialSelectedItem={newPlayer}
        className={SelectStyles.select}
        items={getPlayerOptions()} 
        onChange={handleOnSelectChange}
        placeholder="Filter..."
        itemToString={item => item ? item.text : ''}
        downshiftProps={
          ({
            stateReducer: downshiftStateReducer,
          })
        }
      />
      <Button size="field" type="submit">Add Player</Button>
    </Form>
  );
}

const mapStateToProps = (state) => {
  return {
    sesh: state.sesh,
    players: state.players.players
  };
};

export default connect(mapStateToProps)(AddPlayerToSeshForm);