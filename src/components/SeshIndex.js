import React from 'react';
import { connect } from 'react-redux';
import { loadSeshes, postSesh } from '../actions/seshes_actions';
import SeshList from './SeshList';
import Button from 'carbon-components-react/lib/components/Button';

class SeshIndex extends React.Component {

  constructor(props) {
    super(props);
    props.loadSeshes();
  }

  onSeshFormSubmit = (e) => {
    e.preventDefault();
    this.props.postSesh();
  }

  render() {
    return (
      <>
        <form onSubmit={this.onSeshFormSubmit}>
          <Button type="submit">Create New Sesh</Button>
        </form>
        <SeshList seshes={this.props.seshes} />
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  seshes: state.seshes
});

const mapDispatchToProps = (dispatch) => ({
  loadSeshes: () => dispatch(loadSeshes()),
  postSesh: () => dispatch(postSesh())
});

export default connect(mapStateToProps, mapDispatchToProps)(SeshIndex);
