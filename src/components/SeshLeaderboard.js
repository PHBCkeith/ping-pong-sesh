import React from 'react';
import SeshLeaderboardRow from './SeshLeaderBoardRow';
import { connect } from 'react-redux';
import { DataTable, DataTableSkeleton } from 'carbon-components-react';
import Styles from '../styles/Table.module.css';

const {
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableHeader,
  TableHead,
} = DataTable;

const headers = [
  {
    key: 'name',
    header: 'Name'
  },  
  {
    key: 'wins',
    header: 'Wins'
  },  
  {
    key: 'losses',
    header: 'Losses'
  },
  {
    key: 'remove player',
    header: 'Remove Player'
  }
];

const SeshLeaderboard = ({ sesh, sesh_id }) => {
  const isPreview = !!sesh_id;
  const sesh_status = sesh.status;

  const loadRows = () => 
    sesh.players.map((player) => ({
      id: `${player.id}`
    })) || [];

  const findPlayer = (player_id) => 
    sesh.players.find(player => player.id === player_id);

  return (
    <>
      <DataTable
        rows={loadRows()}
        headers={headers}
        useZebraStyles={true}
        render={({ rows, getTableProps, getHeaderProps, getRowProps }) => (
          <TableContainer title="Sesh DataTable" className={Styles.table}>
            { sesh_status === 'loading' ?
            <DataTableSkeleton 
              headers={headers} 
              rowCount={4}
              zebra={true}
              columnCount={headers.length}
            /> :
            <Table {...getTableProps()}>
              <TableHead>
                <TableRow>
                  {headers.map((header) =>
                    (header.key !== 'remove player' || !isPreview) && 
                    <TableHeader {...getHeaderProps({ header })}>
                      {header.header}
                    </TableHeader>
                  )}
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.map((row, index) =>
                  <SeshLeaderboardRow 
                    key={index} 
                    row={row} 
                    getRowProps={getRowProps} 
                    sesh_id={sesh_id}
                    player={findPlayer(parseInt(row.id))} 
                  />
                )}
              </TableBody>
            </Table>}
          </TableContainer>
        )}
      />
    </>
  );
};

const mapStateToProps = (state, props) => {
  return {
    sesh: state.seshes.seshes.find((sesh) => sesh.id === props.sesh_id) || state.sesh,
  };
};

export default connect(mapStateToProps)(SeshLeaderboard);