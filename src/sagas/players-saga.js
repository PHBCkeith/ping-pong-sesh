import API from "../utils/API";
import { put, call, takeEvery, all, takeLeading } from 'redux-saga/effects';
import { setPlayersState, reloadPlayers } from "../actions/players_actions";
import * as actionTypes from '../actions/actionTypes';

const getPlayers = () => {
  return API.get(`/players`);
};

function* handleLoadPlayers() {
  try {
    let response = yield call(getPlayers);
    yield put(setPlayersState(response.data));
  } catch(err) {
    console.log(err.message);
  }
}

const postPlayer = (player_name) => {
  return API.post('/players', {
    player: {
      name: player_name
    }
  });
}

function* handlePostPlayer({ player_name }) {
  try {
    let response = yield call(postPlayer, player_name);
    if (response.data) {
      yield put(reloadPlayers());
    }
  } catch(err) {
    console.log(err);
  }
}

const deletePlayer = (player_id) => {
  return API.delete(`/players/${player_id}`);
};

function* handleDeletePlayer({ player_id }) {
  try {
    yield call(deletePlayer, player_id);
    yield put(reloadPlayers(player_id));
  } catch(err) {
    console.log(err);
  }
}

function* playersWatcher() {
  yield all([
    takeEvery(actionTypes.LOAD_PLAYERS, handleLoadPlayers),
    takeEvery(actionTypes.RELOAD_PLAYERS, handleLoadPlayers),
    takeLeading(actionTypes.POST_PLAYER, handlePostPlayer),
    takeLeading(actionTypes.DELETE_PLAYER, handleDeletePlayer),
  ]);
}

export default playersWatcher;