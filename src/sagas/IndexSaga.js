import { all } from 'redux-saga/effects';
import playerWatcher from "./player-saga";
import playersWatcher from './players-saga';
import seshWatcher from './sesh-saga';
import seshesWatcher from './seshes-saga';

export default function* IndexSagas() {
  yield all([
    playerWatcher(),
    playersWatcher(),
    seshWatcher(),
    seshesWatcher(),
  ]);
}