import * as actionTypes from '../actions/actionTypes';
import { takeEvery, put, putResolve, call, all, takeLeading } from 'redux-saga/effects';
import { setSeshState, reloadSesh } from '../actions/sesh_actions';
import API from '../utils/API';

const getSesh = (sesh_id) => {
  return API.get(`/seshes/${sesh_id}`);
};

function* handleLoadSesh({ sesh_id }) {
  try {
    let response = yield call(getSesh, sesh_id);
    if (response.data) {
      yield put(setSeshState(response.data));
    }
  } catch(err) {
    console.log(err);
  }
}

const deleteSeshPlayer = (sesh_id, player_id) => {
  return API.delete(`/seshes/${sesh_id}/sesh_players/${player_id}`);
};

function* handleDeletSeshPlayer({ sesh_id, player_id }) {
  try {
    yield call(deleteSeshPlayer, sesh_id, player_id);
    yield put(reloadSesh(sesh_id));
  } catch(err) {
    console.log(err);
  }
}

const postPlayerToSesh = (player_name, sesh_id) => {
  return API.post(`/seshes/${sesh_id}/sesh_players`, {
    sesh_player: {
      name: player_name
    }
  });
};

function* handlePostPlayerToSesh({ player_name, sesh_id }) {
  try {
    let response = yield call(postPlayerToSesh, player_name, sesh_id);
    if (response.data) {
      yield put(reloadSesh(sesh_id));
    }
  } catch(err) {
    console.log(err);
  }
}

const postWin = (sesh_id, winner) => {
  return API.patch(`/seshes/${sesh_id}/sesh_players/${winner.id}`, {
    sesh_player: {
      wins: winner.wins + 1
    }
  });
};

const postLoss = (sesh_id, loser) => {
  return API.patch(`/seshes/${sesh_id}/sesh_players/${loser.id}`, {
    sesh_player: {
      losses: loser.losses + 1
    }
  });
};

function* handlePostMatch({ sesh_id, winner, loser }) {
  try {
    yield call(postWin, sesh_id, winner);
    yield call(postLoss, sesh_id, loser);
    yield putResolve(reloadSesh(sesh_id));
  } catch(err) {
    console.log(err);
  }
}

function* seshWatcher() {
  yield all([
    takeEvery(actionTypes.LOAD_SESH, handleLoadSesh),
    takeEvery(actionTypes.RELOAD_SESH, handleLoadSesh),
    takeLeading(actionTypes.DELETE_SESH_PLAYER, handleDeletSeshPlayer),
    takeLeading(actionTypes.POST_SESH_PLAYER, handlePostPlayerToSesh),
    takeEvery(actionTypes.POST_MATCH, handlePostMatch),
  ]);
}

export default seshWatcher;