import * as actionTypes from '../actions/actionTypes';
import { setSeshesState, reloadSeshes } from '../actions/seshes_actions';
import { takeEvery, put, call, all, takeLeading } from 'redux-saga/effects';
import API from '../utils/API';

const getSeshes = () => {
  return API.get(`/seshes`);
};

function* handleLoadSeshes() {
  try {
    let response = yield call(getSeshes);
    if (response.data) {
      yield put(setSeshesState(response.data));
    }
  } catch(err) {
    console.log(err);
  }
}

const postSesh = () => {
  return API.post(`/seshes`);
}

function* handlePostSesh() {
  try {
    let response = yield call(postSesh);
    if (response.data) {
      yield put(reloadSeshes());
    }
  } catch(err) {
    console.log(err);
  }
}

const deleteSesh = (sesh_id) => {
  return API.delete(`/seshes/${sesh_id}`);
}

function* handleDeleteSesh({ sesh_id }) {
  try {
    yield call(deleteSesh, sesh_id);
    yield put(reloadSeshes());
  } catch(err) {
    console.log(err);
  }
}

function* seshesWatcher() {
  yield all([
    takeEvery(actionTypes.LOAD_SESHES, handleLoadSeshes),
    takeEvery(actionTypes.RELOAD_SESHES, handleLoadSeshes),
    takeLeading(actionTypes.POST_SESH, handlePostSesh),
    takeLeading(actionTypes.DELETE_SESH, handleDeleteSesh)
  ]);
};

export default seshesWatcher;