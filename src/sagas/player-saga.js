import { put, takeEvery, call, all } from "@redux-saga/core/effects";
import { setPlayerState } from "../actions/player_actions";
import API from "../utils/API";
import * as actionTypes from '../actions/actionTypes';

const getPlayer = (player_id) => {
  return API.get(`/players/${player_id}`);
}

function* handleLoadPlayer({ player_id }) {
  try {
    let response = yield call(getPlayer, player_id);
    if (response.data) {
      yield put(setPlayerState(response.data));
    }
  } catch (err) {
    console.log(err);
  }
}

function* playerWatcher() {
  yield all([
    takeEvery(actionTypes.LOAD_PLAYER, handleLoadPlayer)
  ]);
};

export default playerWatcher;