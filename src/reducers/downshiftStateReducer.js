import Downshift from 'downshift';

const downshiftStateReducer = (state, changes) => {
  switch (changes.type) {
    case Downshift.stateChangeTypes.keyDownEscape:
    case Downshift.stateChangeTypes.blurInput:
    case Downshift.stateChangeTypes.blurButton:
    case Downshift.stateChangeTypes.mouseUp:
      if (!state.inputValue) {
        return {
          ...changes,
          inputValue: '',
          selectedItem: {}
        };
      }
      return changes;   
    default:
      return changes;
  }
};

export default downshiftStateReducer;