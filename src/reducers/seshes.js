import * as actionTypes from '../actions/actionTypes';

const seshesReducerDefaultState = {
  status: 'loading',
  seshes: []
};

const seshesReducer = (state = seshesReducerDefaultState, action) => {
  switch (action.type) {
    case actionTypes.LOAD_SESHES:
      return {
        ...state,
        status: 'loading'
      }
    case actionTypes.SET_SESHES_STATE:
      return {
        ...state,
        seshes: action.seshes,
        status: 'completed'
      }
    default:
      return state;
  }
};

export default seshesReducer;