import * as actionTypes from '../actions/actionTypes';

const defaultPlayerState = {
  id: undefined,
  name: '',
  wins: 0,
  losses: 0,
  status: 'loading'
};

const playerReducer = (state = defaultPlayerState, action) => {
  switch(action.type) {
    case actionTypes.LOAD_PLAYER:
      return {
        ...state,
        status: 'loading'
      }
    case actionTypes.SET_PLAYER_STATE:
      return {
        ...state,
        ...action.player,
        status: 'completed'
      }
    case actionTypes.CLEAR_PLAYER_STATE:
      return defaultPlayerState;
    default:
      return state;
  }
};

export default playerReducer;