import * as actionTypes from '../actions/actionTypes';

const seshReducerDefaultState = {
  id: '',
  status: 'loading',
  players: []
};

const seshReducer = (state = seshReducerDefaultState, action) => {
  switch(action.type) {
    case actionTypes.LOAD_SESH:
      return {
        ...state,
        status: 'loading'
      };
    case actionTypes.POST_MATCH:
      return {
        ...state,
        status: 'posting'
      }
    case actionTypes.SET_SESH_STATE:
      return {
        ...state,
        status: 'completed',
        ...action.sesh
      };
    case actionTypes.CLEAR_SESH_STATE:
      return seshReducerDefaultState;  
    default:
      return state;
  }
};

export default seshReducer;