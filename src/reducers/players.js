import * as actionTypes from '../actions/actionTypes';

const playersDefaultState = {
  players: [],
  status: 'loading'
};

const playersReducer = (state = playersDefaultState, action) => {
  switch(action.type) {
    case actionTypes.LOAD_PLAYERS:
      return {
        ...state,
        status: 'loading'
      };
    case actionTypes.SET_PLAYERS_STATE:
      return {
        ...state,
        status: 'completed',
        players: [ ...action.players ]
      };
    default:
      return state;
  }
};

export default playersReducer;