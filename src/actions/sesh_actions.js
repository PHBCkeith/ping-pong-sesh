import * as actionTypes from './actionTypes';

// HANDELED IN SAGAS

export const loadSesh = (sesh_id) => ({
  type: actionTypes.LOAD_SESH,
  sesh_id
});

export const reloadSesh = (sesh_id) => ({
  type: actionTypes.RELOAD_SESH,
  sesh_id
});

export const postSeshPlayer = (player_name, sesh_id) => ({
  type: actionTypes.POST_SESH_PLAYER,
  player_name,
  sesh_id
});

export const deleteSeshPlayer = (sesh_id, player_id) => ({
  type: actionTypes.DELETE_SESH_PLAYER,
  sesh_id,
  player_id
});

export const postMatch = (sesh_id, winner, loser) => ({
  type: actionTypes.POST_MATCH,
  sesh_id,
  winner,
  loser
});

// HANDLED IN REDUCER

export const setSeshState = (sesh) => ({
  type: actionTypes.SET_SESH_STATE,
  sesh
});

export const clearSeshState = () => ({
  type: actionTypes.CLEAR_SESH_STATE
});