import * as actionTypes from './actionTypes';

// HANDLED IN SAGA

export const loadSeshes = () => ({
  type: actionTypes.LOAD_SESHES
});

export const reloadSeshes = () => ({
  type: actionTypes.RELOAD_SESHES
});

export const postSesh = (sesh_id) => ({
  type: actionTypes.POST_SESH,
  sesh_id
});

export const deleteSesh = (sesh_id) => ({
  type: actionTypes.DELETE_SESH,
  sesh_id
});

// HANDLED IN REDUCER

export const setSeshesState = (seshes) => ({
  type: actionTypes.SET_SESHES_STATE,
  seshes
});