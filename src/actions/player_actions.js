import * as actionTypes from './actionTypes';

// HANDLED IN SAGA

export const loadPlayer = (player_id) => ({
  type: actionTypes.LOAD_PLAYER,
  player_id
});

// HANDLED IN REDUCER

export const setPlayerState = (player) => ({
  type: actionTypes.SET_PLAYER_STATE,
  player
});

export const clearPlayerState = () => ({
  type: actionTypes.CLEAR_PLAYER_STATE
});
