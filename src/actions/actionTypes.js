// PLAYER SAGA ACTIONS
export const LOAD_PLAYER =                    'LOAD_PLAYER';  // also in reducer
// PLAYER REDUCER ACTIONS
export const SET_PLAYER_STATE =               'SET_PLAYER_STATE';
export const CLEAR_PLAYER_STATE =             'CLEAR_PLAYER_STATE';

// PLAYERS SAGA ACTIONS
export const LOAD_PLAYERS =                   'LOAD_PLAYERS';  // also in reducer
export const RELOAD_PLAYERS =                 'RELOAD_PLAYERS';
export const POST_PLAYER =                    'POST_PLAYER';
export const DELETE_PLAYER =                  'DELETE_PLAYER';
// PLAYERS REDUCER ACTIONS
export const SET_PLAYERS_STATE =              'SET_PLAYERS_STATE';
  
// SESH SAGA ACTIONS
export const LOAD_SESH =                      'LOAD_SESH';  // also in reducer
export const RELOAD_SESH =                    'RELOAD_SESH';
export const POST_SESH_PLAYER =               'POST_SESH_PLAYER';
export const DELETE_SESH_PLAYER =             'DELETE_SESH_PLAYER';
export const POST_MATCH =                     'POST_MATCH';
// SESH REDUCER ACTIONS
export const SET_SESH_STATE =                 'SET_SESH_STATE';
export const CLEAR_SESH_STATE =               'CLEAR_SESH_STATE';

// SESHES SAGA ACTIONS
export const LOAD_SESHES =                    'LOAD_SESHES';  // also in reducer
export const RELOAD_SESHES =                  'RELOAD_SESHES';
export const POST_SESH =                      'POST_SESH';
export const DELETE_SESH =                    'DELETE_SESH';
// SESHES REDUCER ACTIONS
export const SET_SESHES_STATE =               'SET_SESHES_STATE';