import * as actionTypes from './actionTypes';

// HANDLED IN SAGAS

export const loadPlayers = () => ({
  type: actionTypes.LOAD_PLAYERS,
});

export const reloadPlayers = () => ({
  type: actionTypes.RELOAD_PLAYERS,
});

export const postPlayer = (player_name) => ({
  type: actionTypes.POST_PLAYER,
  player_name,
});

export const deletePlayer = (player_id) => ({
  type: actionTypes.DELETE_PLAYER,
  player_id
});

// HANDLED IN REDUCER

export const setPlayersState = (players) => ({
  type: actionTypes.SET_PLAYERS_STATE,
  players
});